const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/main.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  target: 'electron-main',
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        './src/index.html',
        { from: path.resolve(__dirname, './src/assets'), to: path.resolve(__dirname, 'dist/assets') }, // Copy files from 'assets' folder to 'dist/assets'
      ],
    }),
  ],
};
